#pragma once

#include <iterator>
#include <atomic>
#include <iostream>
#include <cmath>
#include <algorithm>

namespace CoolKidzList {

	template<typename T>
	struct Node {
		T value;
		mutable Node<T>* next;

		Node()
		{
			value = T();
			next = nullptr;
		}

		Node(T val, Node<T>* pointer = nullptr) {
			value = val;
			next = pointer;
		}

		operator T() const { return value; }

		~Node() {
			next = nullptr;
		}

		Node& operator=(const Node& other) {
			if (this != &other) {
				this->value = other.value;
			}
			return *this;
		}

		Node& operator=(const T& value) {
			this->value = value;
			return *this;
		}

		friend bool operator==(const Node<T>& n1, const Node<T>& n2) {
			return n1.value == n2.value;
		}
		friend bool operator<=(const Node<T>& n1, const Node<T>& n2) { return n1.value <= n2.value;}
		friend bool operator<(const Node<T>& n1, const Node<T>& n2) { return n1.value < n2.value; }
		friend bool operator>=(const Node<T>& n1, const Node<T>& n2) { return n1.value >= n2.value; }
		friend bool operator>(const Node<T>& n1, const Node<T>& n2) { return n1.value > n2.value; }

		friend bool operator==(const Node<T>& n1, const T& n2) { return n1.value == n2; }
		friend bool operator<=(const Node<T>& n1, const T& n2) { return n1.value <= n2; }
		friend bool operator<(const Node<T>& n1, const T& n2) { return n1.value < n2; }		
		friend bool operator>=(const Node<T>& n1, const T& n2) { return n1.value >= n2; }
		friend bool operator>(const Node<T>& n1, const T& n2) { return n1.value > n2; }

		friend std::ostream& operator<<(std::ostream& os, const Node& node){ os<<node.value; return os;}

		friend void swap(Node& n1, Node& n2) { std::swap<T>(n1.value, n2.value); };

	};


	template<typename T>
	class SList
	{
	public:

		//==========Iterators class Declarations==========
		using value_type = T;
		using node_type = Node<T>;

		struct SListIterator {
		public:
			using iterator_category = std::forward_iterator_tag;
			using difference_type = size_t;
			using value_type = SList::node_type;
			using pointer = value_type*;
			using reference = value_type&;

			SListIterator() = default;
			SListIterator(const SListIterator& other) : _ptr(other._ptr){}
			SListIterator(pointer ptr) : _ptr(ptr) {};

			reference operator*() const { return *_ptr; }
			pointer operator->() const { return _ptr; }

			SListIterator& operator++() { _ptr = _ptr->next; return *this; }
			SListIterator operator++(int) { SListIterator tmp = *this; ++(*this); return tmp; }
			SListIterator operator+(int right) { 
				SListIterator tmp = *this; 
				for(int i = 0; i<right && tmp->next; i++) tmp++; 
				return tmp;
			}

			friend bool operator==(const SListIterator& a, const SListIterator& b) {return a._ptr == b._ptr; };
			friend bool operator!=(const SListIterator& a, const SListIterator& b) {return a._ptr != b._ptr; };
			
			pointer _ptr;

		};
		struct ConstSListIterator {
		public:
			using iterator_category = std::forward_iterator_tag;
			using difference_type = size_t;
			using value_type = SList::node_type;
			using pointer = const value_type*;
			using reference = const value_type&;

			ConstSListIterator() = default;
			ConstSListIterator(value_type* ptr) : _ptr(ptr) {};
			ConstSListIterator(const SListIterator& it) : _ptr(it._ptr) {};
			ConstSListIterator(const ConstSListIterator& other) : _ptr(other._ptr) {}

			reference operator*() const { return *_ptr; }
			pointer operator->() const { return _ptr; }
			
			ConstSListIterator& operator++() { _ptr = _ptr->next; return *this; }
			ConstSListIterator operator++(int) { ConstSListIterator tmp = *this; ++(*this); return tmp; }
			ConstSListIterator operator+(int right) {
				ConstSListIterator tmp = *this;
				for (int i = 0; i < right && tmp->next; i++) tmp++;
				return tmp;
			}

			friend bool operator==(const ConstSListIterator& a, const ConstSListIterator& b) { return a._ptr == b._ptr; };
			friend bool operator!=(const ConstSListIterator& a, const ConstSListIterator& b) { return a._ptr != b._ptr; };
			value_type* _ptr;
		};

		
		using iterator = SListIterator;
		using const_iterator = ConstSListIterator;

		//==========Constructors==========
		SList();
		SList(std::size_t count, const T& value);
		explicit SList(std::size_t count);
		SList(const SList& other);
		template <class InputIterator, 
			std::enable_if_t<std::_Is_iterator_v<InputIterator>, bool> = true >
		SList(InputIterator first, InputIterator last);
		SList(std::initializer_list<T> init);
		
		//==========Destructor==========
		~SList();

		//==========Copy Assignment==========
		SList& operator=(const SList& other);

		//==========Assignment==========
		void assign(std::size_t count, const T& value);

		template<class InputIterator,
			std::enable_if_t<std::_Is_iterator_v<InputIterator>, bool> = true>
		void assign(InputIterator first, InputIterator last){
			auto listIt = begin();		
			while (first != last && listIt != nullptr) {
				listIt->value = *first;
				listIt++;
				first++;
			}
		}
		
		template<>
		void assign(iterator first, iterator last) {
			auto listIt = begin();

			while (first != last && listIt != nullptr) {
				listIt->value = first->value;
				listIt++;
				first++;
			}
		}

		template<>
		void assign(const_iterator first, const_iterator last) {
			auto listIt = begin();

			while (first != last && listIt != nullptr) {
				listIt->value = first->value;
				listIt++;
				first++;
			}
		}

		//==========Element Access==========
		T& front();
		const T& front() const;

		//==========Iterators==========
		iterator before_begin() noexcept;
		const_iterator before_begin() const noexcept;
		const_iterator cbefore_begin() const noexcept;

		iterator begin() noexcept;
		const_iterator begin() const noexcept;
		const_iterator cbegin() const noexcept;

		iterator end() noexcept;
		const_iterator end() const noexcept;
		const_iterator cend() const noexcept;
		
		//==========Capacity==========
		bool empty() const noexcept;
		std::size_t max_size() const noexcept;

		//==========Modifiers==========
		void clear() noexcept;
		iterator insert_after(const_iterator pos, const T& value);
		iterator erase_after(const_iterator pos);
		void push_front(const T& value);
		void pop_front();
		void resize(size_t count);
		void resize(size_t count, const T& value);
		void swap(SList& other);

		//==========Operations==========
		void merge(SList& other);
		template<class Compare>
		void merge(SList& ohter, Compare cmp);
		//void splice_after()
		void remove(const T& value);
		template<class Predicate>
		void remove_if(Predicate p);
		void reverse() noexcept;
		void unique();
		template<class BinaryPredicate>
		void unique(BinaryPredicate p);
		void sort();
		template<class Compare>
		void sort(Compare comp);

		friend bool operator==(const SList<T>& lhs, const SList<T>& rhs) {
			auto cIteratorLeft = lhs.cbegin();
			auto cIteratorRight = rhs.cbegin();

			bool DifferentElements = false;
			while (cIteratorLeft != nullptr && cIteratorRight != nullptr) {
				DifferentElements = (cIteratorLeft->value != cIteratorRight->value);
				if(DifferentElements) break;

				cIteratorLeft++;
				cIteratorRight++;
			}

			//If I haven't found two different elements, but one list reached the end before the other, that means they are not equal
			bool DifferentSize = (cIteratorLeft == nullptr && cIteratorRight != nullptr) || (cIteratorLeft != nullptr && cIteratorRight == nullptr);

			return !(DifferentElements || DifferentSize);
		};

	private:
		node_type* _head;
		node_type* _tail;
		node_type* _before_head;
	};

#pragma region DEFINITIONS

	template<typename T>
	SList<T>::SList() :
		_head(nullptr),
		_tail(nullptr),
		_before_head(nullptr)

	{}

	template<typename T>
	SList<T>::SList(size_t count, const T& value)
	{
		_head = nullptr;
		_tail = nullptr;
		_before_head = nullptr;
		if (count > 0) {
			_head = new node_type(value);
			node_type* oldNode = _head;
			node_type* newNode = nullptr;
			for (int i = 1; i < count; i++) {
				newNode = new node_type(value);
				oldNode->next = newNode;
				oldNode = newNode;
			}
			_tail = oldNode;
		}
		_before_head = new node_type();
		_before_head->next = _head;
	}

	template<typename T>
	inline SList<T>::SList(std::size_t count)
	{
		_head = nullptr;
		_tail = nullptr;
		_before_head = nullptr;
		if (count > 0) {
			_head = new node_type();
			node_type* oldNode = _head;
			node_type* newNode = nullptr;
			for (int i = 1; i < count; i++) {
				newNode = new node_type();
				oldNode->next = newNode;
				oldNode = newNode;
			}
			_tail = oldNode;
		}
		_before_head = new node_type();
		_before_head->next = _head;
	}

	template<typename T>
	inline SList<T>::SList(const SList& other)
	{
		_head = nullptr;
		_tail = nullptr;
		_before_head = nullptr;
		if (other._head != nullptr) {
			node_type* temp = other._head;
			_head = new node_type(temp->value);
			_tail = _head;
			temp = temp->next;
			while (temp != nullptr) {
				_tail->next = new node_type(temp->value);
				_tail = _tail->next;
				temp = temp->next;
			}
		}
		_before_head = new node_type();
		_before_head->next = _head;
	}

	template<typename T>
	template<class InputIterator, std::enable_if_t<std::_Is_iterator_v<InputIterator>, bool>>
	inline SList<T>::SList(InputIterator first, InputIterator last)
	{
		_head = nullptr;
		_tail = nullptr;
		_before_head = nullptr;

		if (first != last) {
			_head = new node_type(*first);

			first++;

			node_type* tmp = _head;
			node_type* newTmp = nullptr;

			while (first != last) {
				newTmp = new node_type(*first);
				tmp->next = newTmp;
				tmp = newTmp;
				newTmp = nullptr;
				first++;
			}

			_tail = tmp;

		}

		_before_head = new node_type();
		_before_head->next = _head;
	}

	template<typename T>
	inline SList<T>::SList(std::initializer_list<T> init)
	{
		_head = nullptr;
		_tail = nullptr;
		_before_head = nullptr;
		auto initIt = init.begin();
		if (initIt != nullptr) {
			_head = new node_type(*initIt);
			_tail = _head;
			_head->next = nullptr;
		}

		node_type* oldTmp = _head;
		node_type* tmp;

		initIt++;

		for (int i = 1; i < init.size(); i++)
		{
			tmp = new node_type(*initIt);
			tmp->next = nullptr;
			oldTmp->next = tmp;
			_tail = tmp;
			oldTmp = tmp;
			initIt++;
		}

		_before_head = new node_type();
		_before_head->next = _head;
	}

	template<typename T>
	SList<T>::~SList() 
	{
		clear();
	}

	template<typename T>
	inline SList<T>& SList<T>::operator=(const SList<T>& other)
	{
		if (this != &other) {
			clear();
			_head = nullptr;
			_tail = nullptr;
			if (other._head != nullptr) {
				node_type* temp = other._head;
				_head = new node_type(temp->value);
				_tail = _head;
				temp = temp->next;
				while (temp != nullptr) {
					_tail->next = new node_type(temp->value);
					_tail = _tail->next;
					temp = temp->next;
				}
			}
		}
		return *this;
	}

	template<typename T>
	inline void SList<T>::assign(std::size_t count, const T& value)
	{
		clear();
		_head = nullptr;
		_tail = nullptr;
		if (count > 0) {
			_head = new node_type(value);
			node_type* oldNode = _head;
			node_type* newNode = nullptr;
			for (int i = 1; i < count; i++) {
				newNode = new node_type(value);
				oldNode->next = newNode;
				oldNode = newNode;
			}
			_tail = oldNode;
		}
	}


	template<typename T>
	T& SList<T>::front() {
		return _head->value;
	}
	template<typename T>
	inline const T& SList<T>::front() const
	{
		return _head->value;
	}
	template<typename T>
	inline typename SList<T>::iterator SList<T>::before_begin() noexcept
	{
		_before_head->next = _head;
		return iterator(_before_head);
	}
	template<typename T>
	inline typename SList<T>::const_iterator SList<T>::before_begin() const noexcept
	{
		_before_head->next = _head;
		return const_iterator(_before_head);
	}
	template<typename T>
	inline typename SList<T>::const_iterator SList<T>::cbefore_begin() const noexcept
	{
		return before_begin();
	}

	template<typename T>
	inline typename SList<T>::iterator SList<T>::begin() noexcept{
		typename SList<T>::iterator it(_head);
		return it;
	}

	template<typename T>
	inline typename SList<T>::const_iterator SList<T>::begin() const noexcept
	{
		typename SList<T>::const_iterator it(_head);
		return it;
	}

	template<typename T>
	inline typename SList<T>::const_iterator SList<T>::cbegin() const noexcept {
		return begin();
	}

	template<typename T>
	inline typename SList<T>::iterator SList<T>::end() noexcept
	{
		typename SList<T>::iterator it(_tail->next);
		return it;
	}

	template<typename T>
	inline typename SList<T>::const_iterator SList<T>::end() const noexcept
	{
		typename SList<T>::const_iterator it(_tail->next);
		return it;
	}

	template<typename T>
	inline typename  SList<T>::const_iterator SList<T>::cend() const noexcept
	{
		return end();
	}

	template<typename T>
	inline bool SList<T>::empty() const noexcept
	{
		return (_head == nullptr);
	}

	template<typename T>
	inline std::size_t SList<T>::max_size() const noexcept
	{
		return static_cast<size_t>(pow(2,(sizeof(T*)*8))-1);
	}

	template<typename T>
	inline void SList<T>::clear() noexcept
	{
		node_type* toFree = _head;
		node_type* toFreeNext = _head;
		while (toFreeNext != nullptr) {
			toFree = toFreeNext;
			toFreeNext = toFree->next;
			toFree->next = nullptr;
			delete(toFree);
			toFree = nullptr;
		}
		_tail = nullptr;

		if (_before_head != nullptr) {
			_before_head->next = nullptr;
			delete(_before_head);
			_before_head = nullptr;
		}

	}

	template<typename T>
	inline typename SList<T>::iterator SList<T>::insert_after(const_iterator pos, const T& value)
	{
		node_type* newNode = new node_type(value);
		newNode->next = pos->next;
		pos->next = newNode;

		return iterator(newNode);
	}

	template<typename T>
	inline typename SList<T>::iterator SList<T>::erase_after(const_iterator pos)
	{
		node_type* tmp = pos->next;

		if (tmp != nullptr) {

			pos->next = tmp->next;

			if (tmp == _head) {
				_head = _head->next;
				_before_head->next = _head;
			}

			delete(tmp);

			if (pos->next == nullptr) {
				_tail = pos._ptr;
			}

			return iterator(pos->next);
		}
		else
			return end();
	}

	template <typename T>
	void SList<T>::push_front(const T& value) 
	{
		node_type* nextNode = new node_type(value);
		nextNode->next = _head;
		_head = nextNode;
		if (_head->next == nullptr)
			_tail = _head;
	}
	template<typename T>
	inline void SList<T>::pop_front()
	{
		node_type* nodeToDestroy = _head;
		if (_tail == _head) {
			_tail = _tail->next;
		}
		_head = _head->next;

		delete(nodeToDestroy);
	}
	template<typename T>
	inline void SList<T>::resize(size_t count)
	{
		resize(count, T());
	}
	template<typename T>
	inline void SList<T>::resize(size_t count, const T& value)
	{
		if (count > 0) {
			node_type* tmp = _head;
			int i;
			for (i = 0; i < count-1; i++) {
				if (tmp->next != nullptr) {
					tmp = tmp->next;
				}
				else {
					tmp->next = new node_type(value);
					tmp->next->next = nullptr;
					tmp = tmp->next;
				}
			}
			_tail = tmp;

			node_type* tmpToDelete = tmp->next;
			tmp = tmp->next;

			while (tmp != nullptr) {
				tmpToDelete = tmp;
				tmp = tmp->next;
				delete(tmpToDelete);
			}

			_tail->next = nullptr;

		}
		else {
			clear();
		}
	}

	template<typename T>
	inline void SList<T>::swap(SList& other)
	{
		SList<T> tmp;
		tmp = other;
		other = *this;
		*this = tmp;
	}
	
	template<typename T>
	void SList<T>::remove(const T& value) {
		
		struct Predicate {
			const T& value_to_compare;
			Predicate(const T& value) : value_to_compare(value) {}

			bool operator()(const T& op2) const {
				return value_to_compare == op2;
			}
		};

		Predicate p(value);
		remove_if<Predicate>(p);
	}

	template<typename T>
	template<class Predicate>
	inline void SList<T>::remove_if(Predicate p)
	{
		node_type* tmp = _head;
		node_type* lastTmp = nullptr;
		node_type* tmpToDelete;
		while (tmp != nullptr) {
			if (p(tmp->value)) {

				if (_head == tmp) {
					_head = _head->next;
				}

				if (_tail == tmp) {
					_tail = lastTmp;
				}
				
				tmpToDelete = tmp;

				tmp = tmp->next;

				delete(tmpToDelete);

				if (lastTmp != nullptr) {
					lastTmp->next = tmp;
				}

			}
			else {
				lastTmp = tmp;
				tmp = tmp->next;
			}
		}
	}

	template<typename T>
	inline void SList<T>::reverse() noexcept
	{
		node_type* current = _head;
		node_type* prev = nullptr;
		node_type* next = nullptr;
		while (current != nullptr) {
			next = current->next;

			current->next = prev;
			prev = current;

			current = next;

		}

		_tail = _head;
		_head = prev;

	}

	template<typename T>
	inline void SList<T>::unique()
	{
		//LAMBDA!!!
		auto p = [](int a, int b)-> bool {return a == b; };
		unique(p);
	}

	template<typename T>
	template<class BinaryPredicate>
	void CoolKidzList::SList<T>::unique(BinaryPredicate p)
	{
		if(empty()) return;

		iterator it = begin();
		while (it != end()) {
			if (it->next && p(it->value, it->next->value)) {
				erase_after(it);
			}
			else {
				it++;
			}
		}
	}
	template<typename T>
	void SList<T>::sort() {
		auto _LESS = [](const node_type& a,const node_type& b)->bool{return a<=b;};
		sort(_LESS);
	}

	template<typename T>
	template<class Compare>
	void SList<T>::sort(Compare comp) {
		//Sorts in O(nLog(n)) time thanks to std::sort, and taking up O(n) space
		size_t listSize = std::distance(begin(), end());
		T* AsArray = new T [listSize];

		int i = 0;
		for (auto it = cbegin(); it != nullptr; it++) {
			AsArray[i] = it->value;
			i++;
		}

		std::sort(AsArray, AsArray + listSize, comp);
		i = 0;
		for (auto it = begin(); it != nullptr; it++) {
			it->value = AsArray[i];
			i++;
		}

		delete[] AsArray;
	}

	template<typename T>
	void SList<T>::merge(SList<T>& other) {
		auto _LESS = [](const T& a,const T& b)->bool{return a<=b;};
		merge(other, _LESS);
	}

	template<typename T>
	template<class Compare>
	void SList<T>::merge(SList<T>& other, Compare comp) {
		this->_tail = other._tail;
		
		auto it = begin();
		for(; it->next != nullptr; it++); //Scroll to one before end
		it->next = other._head;

		other._head = nullptr;
		other._tail = nullptr;
		this->sort();
	}
#pragma endregion DEFINITIONS

};

namespace std {
	using CoolKidzList::SList;

	template<typename T>
	void swap(SList<T>& lhs, SList<T>& rhs) noexcept {lhs.swap(rhs);};
}