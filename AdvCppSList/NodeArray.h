#pragma once

template<typename T>
	struct NodeArray {
		T value;
		mutable int nextIndex;

		NodeArray()
		{
			value = T();
			nextIndex = POSEND;
		}

		NodeArray(const NodeArray& other)
		{
			value = other.value;
			nextIndex = other.nextIndex;
		}

		NodeArray(T val, int pos = POSEND) {
			value = val;
			nextIndex = pos;
		}

		~NodeArray() {
			nextIndex = -1;
		}

		NodeArray& operator=(const NodeArray& other)
		{
			if (this != &other)
			{
				value = other.value;
				nextIndex = other.nextIndex;
			}
			return *this;
		}

		NodeArray& operator=(const T& value) {
			this->value = value;
			return *this;
		}

		operator T() const{
			return this->value;
		}

		friend bool operator==(const NodeArray<T>& n1, const NodeArray<T>& n2) {
			return n1.value == n2.value;
		}
		friend bool operator<=(const NodeArray<T>& n1, const NodeArray<T>& n2) { return n1.value <= n2.value; }
		friend bool operator<(const NodeArray<T>& n1, const NodeArray<T>& n2) { return n1.value < n2.value; }
		friend bool operator>=(const NodeArray<T>& n1, const NodeArray<T>& n2) { return n1.value >= n2.value; }
		friend bool operator>(const NodeArray<T>& n1, const NodeArray<T>& n2) { return n1.value > n2.value; }

		friend bool operator==(const NodeArray<T>& n1, const T& n2) { return n1.value == n2; }
		friend bool operator<=(const NodeArray<T>& n1, const T& n2) { return n1.value <= n2; }
		friend bool operator<(const NodeArray<T>& n1, const T& n2) { return n1.value < n2; }
		friend bool operator>=(const NodeArray<T>& n1, const T& n2) { return n1.value >= n2; }
		friend bool operator>(const NodeArray<T>& n1, const T& n2) { return n1.value > n2; }

		friend std::ostream& operator<<(std::ostream& os, const NodeArray& node) { os << node.value; return os; }

		friend void swap(NodeArray& n1, NodeArray& n2) { std::swap<T>(n1.value, n2.value); std::swap<int>(n1.nextIndex, n2.nextIndex); };


		static constexpr int POSBEFOREHEAD = 0;
		static constexpr int POSDELETED = -1;
		static constexpr int POSEND = -2;
	};

