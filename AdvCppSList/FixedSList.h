#pragma once

#include<stack>
#include <iostream>
#include <algorithm>
#include "SListArray.h"

namespace CoolKidzList {
	template<typename T, size_t size>
	class FixedSList
	{
	public:
		//==========Iterators class Declarations==========
		using value_type = T; //Needed by STL

		using node_type = NodeArray<T>;
		static constexpr int POSBEFOREHEAD = NodeArray<T>::POSBEFOREHEAD;
		static constexpr int POSDELETED = NodeArray<T>::POSDELETED;
		static constexpr int POSEND = NodeArray<T>::POSEND;

		struct FixedSListIterator {
			using iterator_category = std::forward_iterator_tag;
			using difference_type = size_t;
			using value_type = FixedSList::node_type;
			using pointer = value_type*;
			using reference = FixedSList::value_type&;

			FixedSListIterator() = delete;
			FixedSListIterator(NodeArray<T>* pointed_vector) :vector(pointed_vector), position(0) {};
			FixedSListIterator(NodeArray<T>* pointed_vector, int pos) :vector(pointed_vector), position(pos) {};
			FixedSListIterator(const FixedSListIterator& other) : vector(other.vector), position(other.position) {}

			FixedSListIterator& operator=(const FixedSListIterator& other) {
				if (*this != other)
				{
					this->vector = other.vector;
					this->position = other.position;
				}
				return *this;
			}

			reference operator*() const { return (vector+position)->value; }
			pointer operator->() const { return (vector+position); }

			FixedSListIterator& operator++() { position = (*this)->nextIndex; return *this; }
			FixedSListIterator operator++(int) { FixedSListIterator tmp = *this; ++(*this); return tmp; }
			FixedSListIterator operator+(int right) {
				FixedSListIterator tmp = *this;
				for (int i = 0; i < right; i++) tmp++;
				return tmp;
			}

			friend bool operator==(const FixedSListIterator& a, const FixedSListIterator& b) {
				//Two iterators are equal if they point to the same vector at the same position
				return (a.vector == b.vector) && (a.position == b.position);
			};
			friend bool operator!=(const FixedSListIterator& a, const FixedSListIterator& b) {
				return !(a == b);
			}

			NodeArray<T>* vector;
			int position;
		};

		struct FixedSListConstIterator {
			using iterator_category = std::forward_iterator_tag;
			using difference_type = size_t;
			using value_type = FixedSList::node_type;
			using pointer = const value_type*;
			using reference = const FixedSList::value_type&;

			FixedSListConstIterator() = delete;
			FixedSListConstIterator(const NodeArray<T>* pointed_vector) :vector(pointed_vector), position(0) {};
			FixedSListConstIterator(const NodeArray<T>* pointed_vector, int pos) :vector(pointed_vector), position(pos) {};
			FixedSListConstIterator(const FixedSListConstIterator& other) : vector(other.vector), position(other.position) {}
			FixedSListConstIterator(const FixedSListIterator& other) : vector(other.vector), position(other.position) {}

			FixedSListConstIterator& operator=(const FixedSListConstIterator& other) {
				if (*this != other)
				{
					this->vector = other.vector;
					this->position = other.position;
				}
				return *this;
			}

			reference operator*() const { return (vector + position)->value; }
			pointer operator->() const { return (vector + position); }

			FixedSListConstIterator& operator++() { position = (*this)->nextIndex; return *this; }
			FixedSListConstIterator operator++(int) { FixedSListConstIterator tmp = *this; ++(*this); return tmp; }
			FixedSListConstIterator operator+(int right) {
				FixedSListConstIterator tmp = *this;
				for (int i = 0; i < right; i++) tmp++;
				return tmp;
			}

			friend bool operator==(const FixedSListConstIterator& a, const FixedSListConstIterator& b) {
				return (a.vector == b.vector) && (a.position == b.position);
			};
			friend bool operator!=(const FixedSListConstIterator& a, const FixedSListConstIterator& b) {
				return !(a == b);
			}

			const NodeArray<T>* vector;
			int position;
		};

		using iterator = FixedSListIterator;
		using const_iterator = FixedSListConstIterator;


		//==========Constructors==========
		FixedSList();
		FixedSList(size_t count, const T& value);
		template <class InputIterator,
			std::enable_if_t<std::_Is_iterator_v<InputIterator>, bool> = true >
		FixedSList(InputIterator first, InputIterator last);
		FixedSList(std::initializer_list<T> init);
		FixedSList(const FixedSList& other);

		//==========Destructor==========
		~FixedSList() = default;

		//==========Iterators==========
		iterator before_begin() noexcept { iterator beforeBeginIt = iterator(m_data, POSBEFOREHEAD); return beforeBeginIt; };
		const_iterator before_begin() const noexcept { const_iterator beforeBeginIt = const_iterator(m_data, POSBEFOREHEAD); return beforeBeginIt; };
		const_iterator cbefore_begin() const noexcept { return before_begin(); };

		inline iterator begin() noexcept { iterator beginIt = iterator(m_data, posHead); return beginIt; };
		inline const_iterator begin() const noexcept { const_iterator beginIt = const_iterator(m_data, posHead); return beginIt; };
		inline const_iterator cbegin() const noexcept { return begin(); };

		inline iterator end() noexcept { iterator endIt = iterator(m_data, POSEND); return endIt; };
		inline const_iterator end() const noexcept { const_iterator endIt(m_data, POSEND); return endIt; };
		inline const_iterator cend() const noexcept { return end(); };

		//==========Modifiers==========
		void clear() noexcept;
		iterator insert_after(const_iterator pos, const T& value);
		iterator erase_after(const_iterator pos);
		void push_front(const T& value);
		void pop_front();
		void swap(FixedSList& other);

		//=========Operations==========
		void sort();
		template<class Compare>
		void sort(Compare comp);

		//=========Operators==========
		friend std::ostream& operator<<(std::ostream& os, const FixedSList& list)
		{
			const_iterator it = list.begin();
			for (; it != list.end(); it++) {
				std::cout << *it << " ";
			}
			return os;
		}
	private:
		std::stack<int> freePositionInVector;
		static constexpr int _capacity = size + 1;
		NodeArray<T> m_data[_capacity] = {};	//+1 to contain before head
		int posHead;
		int posTail;
		

		//=========UTILITY=============
		int _size(){ return int(size - freePositionInVector.size()); }
	};


#pragma region DEFINITIONS

	template<typename T, size_t size>
	FixedSList<T, size>::FixedSList() :
		posHead(-1),
		posTail(1)
	{
		m_data[0].value = T(); 
		m_data[0].nextIndex = POSEND;
		for (int i = 1; i<_capacity; i++)
		{
			m_data[i].nextIndex = POSDELETED;
			freePositionInVector.push(i);
		}
	}

	template<typename T, size_t size>
	FixedSList<T, size>::FixedSList(size_t count, const T& value) : FixedSList() {
		for (int i = 0; i < count; i++) {
			push_front(value);
		}
	}

	template<typename T, size_t size>
	template<class InputIterator, std::enable_if_t<std::_Is_iterator_v<InputIterator>, bool>>
	FixedSList<T, size>::FixedSList(InputIterator first, InputIterator last) : FixedSList()
	{
		auto it = cbefore_begin();
		int insertedPosition = 1;
		for (; first != last; first++, it++) {
			auto insertedIt = insert_after(it, *first);
			insertedPosition = insertedIt.position;
		}
		posHead = cbefore_begin()->nextIndex;
		posTail = insertedPosition;
	}

	template<typename T, size_t size>
	FixedSList<T, size>::FixedSList(std::initializer_list<T> init) : FixedSList(init.begin(), init.end()) {}

	template<typename T, size_t size>
	void FixedSList<T, size>::clear() noexcept {
		auto it = begin();
		while (it != end()) {
			freePositionInVector.push(it.position);
			auto next = it + 1;
			it->nextIndex = -1;
			it = next;
		}

		posHead = POSEND;
		posTail = 1;
	}

	template<typename T, size_t size>
	FixedSList<T, size>::FixedSList(const FixedSList<T, size>& other) :
		posHead(other.posHead),
		posTail(other.posTail),
		freePositionInVector(other.freePositionInVector)
	{
		std::copy(std::begin(other.m_data), std::end(other.m_data), std::begin(m_data));
	}

	template<typename T, size_t size>
	inline typename FixedSList<T, size>::iterator FixedSList<T, size>::insert_after(const_iterator pos, const T& value)
	{

		int posToInsert = POSEND;
		if (freePositionInVector.size() > 0) {
			posToInsert = freePositionInVector.top();
			freePositionInVector.pop();
			m_data[posToInsert].value = value;
		}
		else {
			return end();
		}

		if (pos.position == 0) {
			posHead = posToInsert;
		}

		m_data[posToInsert].nextIndex = pos->nextIndex;
		pos->nextIndex = posToInsert;

		if (m_data[posToInsert].nextIndex == POSEND)
			posTail = posToInsert;

		return iterator(m_data, posToInsert);
	}

	template<typename T, size_t size>
	inline typename FixedSList<T, size>::iterator FixedSList<T, size>::erase_after(const_iterator pos)
	{
		if (pos->nextIndex != POSEND) {
			auto nextNode = pos + 1;
			freePositionInVector.push(pos->nextIndex);
			if (pos->nextIndex == posHead)
				posHead = nextNode->nextIndex;
			if (pos->nextIndex == posTail)
				posTail = pos.position;
			pos->nextIndex = nextNode->nextIndex;
			nextNode->nextIndex = POSDELETED;
		};

		return iterator(m_data, pos->nextIndex);
	}

	template <typename T, size_t size>
	void FixedSList<T, size>::push_front(const T& value)
	{
		if (freePositionInVector.size() > 0)
		{
			int posToInsert = freePositionInVector.top();
			freePositionInVector.pop();
			m_data[posToInsert].value = value;
			
			if (posHead == -1) {
				m_data[posToInsert].nextIndex = -2;
				posTail = posToInsert;
			}
			else {
				m_data[posToInsert].nextIndex = posHead;
			}
			posHead = posToInsert;
			m_data[POSBEFOREHEAD].nextIndex = posHead;
		}
		return;
	}

	template<typename T, size_t size>
	inline void FixedSList<T, size>::pop_front()
	{
		if (posHead != -1) {
			freePositionInVector.push(posHead);
			int posToGo = m_data[posHead].nextIndex;
			m_data[posHead].nextIndex = -1;
			posHead = posToGo;
			if (posHead != -1 && m_data[posHead].nextIndex == POSEND) {
				posTail = posHead;
			}
			m_data[POSBEFOREHEAD].nextIndex = posHead;
		}
	}

	template<typename T, size_t size>
	void FixedSList<T, size>::swap(FixedSList<T, size>& other) {
		std::swap(m_data, other.m_data);
		std::swap(freePositionInVector, other.freePositionInVector);
		std::swap(posHead, other.posHead);
		std::swap(posTail, other.posTail);
	}

	template<typename T, size_t size>
	void FixedSList<T, size>::sort() {
		auto _LESS = [](const node_type& a, const node_type& b)->bool {return a < b; };
		sort(_LESS);
	}

	template<typename T, size_t size>
	template<class Compare>
	void FixedSList<T, size>::sort(Compare comp) {
		if (_size() <= 1) {
			return;
		}
		std::sort(std::begin(m_data) + 1, std::end(m_data), comp);
		auto itToUpdate = std::cbegin(m_data);
		auto itToNext = itToUpdate;
		while (itToUpdate != std::cend(m_data) && itToNext != std::end(m_data)) {

			if (itToUpdate->nextIndex == POSDELETED) { itToUpdate++; itToNext = itToUpdate; continue; }
			if (itToNext->nextIndex == POSDELETED) { itToNext++; continue; };

			itToUpdate->nextIndex = int(std::distance(std::cbegin(m_data), itToNext));
			itToUpdate = itToNext;
			itToNext++;
		}

		posHead = cbefore_begin()->nextIndex;
		posTail = int(std::distance(std::cbegin(m_data)+1, itToNext));
		itToUpdate->nextIndex = POSEND;

	}

#pragma endregion DEFINITIONS
}

namespace std {
	using CoolKidzList::FixedSList;

	template<typename T, size_t size>
	void swap(FixedSList<T, size>& lhs, FixedSList<T, size>& rhs) noexcept { lhs.swap(rhs); };

}