#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>
#include "SList.h"
#include "SListArray.h"
#include "FixedSList.h"


bool isMoreThanThreeCharacters(std::string stringToCheck) {
    return (stringToCheck.length() > 3);
}

void SListTesting();
void SListArrayTesting();
void FixedSListTesting();


int main()
{
    std::cout<<"================SList Tests================"<<std::endl;
    SListTesting();
	std::cout <<std::endl<<"================SList Array Tests================" << std::endl;
    SListArrayTesting();
	std::cout << std::endl << "================FixedSList Tests================" << std::endl;
    FixedSListTesting();
    return 0;
}


void SListTesting() {
    //========SETUP=========
    /*
        new SList of various types
        fill lists with values
    */
    using namespace CoolKidzList;

    std::cout << "======SETUP AND MEMBER OPERATIONS=======" << std::endl;

    SList<int> lista = SList<int>();

    lista.push_front(1);
    std::cout << lista.front() << " ";
    lista.push_front(2);
    std::cout << lista.front() << std::endl;



    //=========USAGE============
    /*
        Use the methods given by the various implementations here and test for results here
    */

    std::cout << "WITH ITERATORS" << std::endl;
    SList<int>::iterator it = lista.begin();
    std::cout << it->value << " ";
    it++;
    std::cout << it->value << std::endl;

    SList<float> lista2 = SList<float>(3, 3.14f);
    std::cout << "WITH CONST ITERATOR" << std::endl;
    SList<float>::const_iterator cit = lista2.cbegin();
    std::cout << cit->value << " ";
    cit++;
    std::cout << cit->value << std::endl;

    std::cout << "PUSH FRONT AND PRINT WITH ITERATORS" << std::endl;
    lista2.push_front(6.9f);
    SList<float>::iterator iterator = lista2.begin();
    while (iterator != lista2.end()) {
        std::cout << iterator->value << " ";
        iterator++;
    }
    std::cout << std::endl;

    std::cout << "Calling pop-front" << std::endl;
    lista2.pop_front();

    iterator = lista2.begin();
    while (iterator != nullptr) {
        std::cout << iterator->value << " ";
        iterator++;
    }
    std::cout << std::endl;

    std::cout << "Default initialization with size 10" << std::endl;
    SList<int> lista3(10);
    SList<int>::iterator iterator2 = lista3.begin();
    while (iterator2 != nullptr) {
        std::cout << iterator2->value << " ";
        iterator2++;
    }
    std::cout << std::endl;

    std::cout << "COPY CONSTRUCTION" << std::endl;
    SList<int> lista4 = lista;
    iterator2 = lista4.begin();

    while (iterator2 != nullptr) {
        std::cout << iterator2->value << " ";
        iterator2++;
    }
    std::cout << std::endl;

    std::cout << "ASSIGN WITH ITERATORS" << std::endl;
    int test[] = { 11, 12, 13, 14, 15, 16 };
    lista4.assign(std::begin(test), std::end(test));
    for (const SList<int>::node_type& node : lista4)
        std::cout << node << " ";
    std::cout << std::endl;


    std::cout << "Assign(3,4), print, then Assign(0,0), empty, max size" << std::endl;
    lista3.assign(3, 4);
    iterator2 = lista3.begin();
    while (iterator2 != nullptr) {
        std::cout << iterator2->value << " ";
        iterator2++;
    }

    lista3.assign(0, 0);

    std::cout << "\n" << "List is empty? " << lista3.empty() << "\n";

    std::cout << "Max Size? " << lista3.max_size() << std::endl;

    std::cout << "Initialization list constuction " << std::endl;
    SList<std::string> arizonaRangers = { "Degenerates", "like", "you", "belong", "on", "a", "cross" };

    auto iteratoreMojave = arizonaRangers.begin();

    while (iteratoreMojave != nullptr) {
        std::cout << iteratoreMojave->value << " ";
        iteratoreMojave++;
    }
    std::cout << std::endl << "RESIZE 3" << std::endl;
    arizonaRangers.resize(3);

    iteratoreMojave = arizonaRangers.begin();

    while (iteratoreMojave != nullptr) {
        std::cout << iteratoreMojave->value << " ";
        iteratoreMojave++;
    }

    std::cout << std::endl << "Resize(2, \"bigiron\")" << std::endl;
    arizonaRangers.resize(2, "bigiron");

    iteratoreMojave = arizonaRangers.begin();

    while (iteratoreMojave != nullptr) {
        std::cout << iteratoreMojave->value << " ";
        iteratoreMojave++;
    }

    std::cout << std::endl;
    std::cout << "Resize(10, \"pip\"), the remove all 3+ characters string" << std::endl;
    arizonaRangers.resize(10, "pip");
    arizonaRangers.remove_if(isMoreThanThreeCharacters);


    iteratoreMojave = arizonaRangers.begin();

    while (iteratoreMojave != nullptr) {
        std::cout << iteratoreMojave->value << " ";
        iteratoreMojave++;
    }

    std::cout << std::endl;

    std::cout << "Initialization with iterator of the previous list" << std::endl;

    SList<std::string> nuovaLista(arizonaRangers.begin(), arizonaRangers.end());

    iteratoreMojave = nuovaLista.begin();

    while (iteratoreMojave != nullptr) {
        std::cout << iteratoreMojave->value << " ";
        iteratoreMojave++;
    }

    std::cout << std::endl;

    std::cout << "Create two lists, then swap them" << std::endl;
    SList<int> test1 = { 2,1,43 };
    SList<int> test2 = { 2,2,2 };

    test1.swap(test2);

    SList<int>::iterator iteratore = test1.begin();
    SList<int>::iterator iteratore2 = test2.begin();

    std::cout << "List 1 after swap: ";
    while (iteratore != nullptr) {
        std::cout << iteratore->value << " ";
        iteratore++;
    }
    std::cout << std::endl << "List 2 after swap: ";
    while (iteratore2 != nullptr) {
        std::cout << iteratore2->value << " ";
        iteratore2++;
    }
    std::cout << std::endl;

    std::cout << "UNIQUE" << std::endl;
    lista = { 1, 2, 2, 3, 3, 4, 5 };
    lista.unique();
    auto it1 = lista.begin();
    while (it1 != nullptr) {
        std::cout << it1->value << " ";
        it1++;
    }
    std::cout << std::endl;

    auto sameIntegralPart = [](double a, double b)-> bool {
        return (int(a) == int(b));
    };

    std::cout << "WEIRD UNIQUE" << std::endl;

    SList<double> weirdList = { 3.14, 3.15, 4.145, 4.5124 };
    weirdList.unique(sameIntegralPart);

    SList<double>::SListIterator iteratoreDoppio = weirdList.begin();
    while (iteratoreDoppio != nullptr) {
        std::cout << iteratoreDoppio->value << " ";
        iteratoreDoppio++;
    }
    std::cout << std::endl;
    std::cout << std::endl;

    SList<std::string> ncrPatroller = { "Patrolling","the","Mojave","almost","make","you","wish","for","a","nuclear","winter" };

    iteratoreMojave = ncrPatroller.begin();

    while (iteratoreMojave != nullptr) {
        std::cout << iteratoreMojave->value << " ";
        iteratoreMojave++;
    }

    ncrPatroller.reverse();

    std::cout << std::endl;
    std::cout << "NOW REVERTED" << std::endl;

    iteratoreMojave = ncrPatroller.begin();

    while (iteratoreMojave != nullptr) {
        std::cout << iteratoreMojave->value << " ";
        iteratoreMojave++;
    }

    std::cout << std::endl;

    SList<int> compare1 = { 5, 6, 7, 8 };
    SList<int> compare2 = { 5, 6, 7, 8 };
    SList<int> compare3 = { 6, 7, 8 };
    SList<int> compare4 = { 5, 6, 7 };

    std::cout << "COMPARING {5, 6, 7, 8} and {5, 6, 7, 8}: " << ((compare1 == compare2) ? "true" : "false") << std::endl;
    std::cout << "COMPARING {5, 6, 7, 8} and {6, 7, 8}: " << ((compare1 == compare3) ? "true" : "false") << std::endl;
    std::cout << "COMPARING {5, 6, 7, 8} and {5, 6, 7}: " << ((compare1 == compare4) ? "true" : "false") << std::endl;

    //cit->value = 3; This gives an error: iterator is unmodifiable

    std::cout << "SORTING {4, 3, 5, 2, 1}" << std::endl;
    SList<int> listToSort = { 4, 3, 5, 2, 1 };
    listToSort.sort();
    auto toSortIt = listToSort.cbegin();
    while (toSortIt != nullptr) {
        std::cout << toSortIt->value << " ";
        toSortIt++;
    }
    std::cout << std::endl;

    std::cout << "AND MERGING {6, 7, 8, 0}" << std::endl;
    SList<int> listToMerge = { 6, 7, 8, 0 };
    listToSort.merge(listToMerge);
    toSortIt = listToSort.cbegin();
    while (toSortIt != nullptr) {
        std::cout << toSortIt->value << " ";
        toSortIt++;
    }
    std::cout << std::endl;


    std::cout << std::endl;
    std::cout << std::endl;

    //=======STD::ALGORITHM=======
    /*
        Try std algorithm here
    */
    std::cout << "=======STD::ALGORITHM=======" << std::endl;
    std::cout << "std::fill" << std::endl;
    std::fill(test1.begin(), test1.end(), 4);

    iteratore = test1.begin();
    while (iteratore != nullptr) {
        std::cout << iteratore->value << " ";
        iteratore++;
    }
    std::cout << std::endl;

    std::cout << "std::all_of < 5 on {1.2f, 3.2f, 4.9999f, 0.f}" << std::endl;
    SList<float> listAllLessThan5 = { 1.2f, 3.2f, 4.9999f, 0.f };
    auto LessThan5Lambda = ([](SList<float>::node_type val)->bool {return val < 5.0f; });
    std::cout << (std::all_of(listAllLessThan5.begin(), listAllLessThan5.end(), LessThan5Lambda) ? "true" : "false");

    std::cout << std::endl;

    std::cout << "std::search first > than 4.f" << std::endl;
    auto moreThan4Lambda = ([](SList<float>::node_type val)->bool { return val > 4.0f; });
    std::cout << "Element found: " << *std::find_if(listAllLessThan5.begin(), listAllLessThan5.end(), moreThan4Lambda);
    std::cout << std::endl;

    std::cout << "std::copy, from list to vector" << std::endl;
    std::vector<SList<float>::node_type> destinationVector;
    std::copy(listAllLessThan5.begin(), listAllLessThan5.end(), std::back_inserter(destinationVector));
    for (const SList<float>::node_type& node : destinationVector) {
        std::cout << node.value << " ";
    }
    std::cout << std::endl;

    std::cout << "std::partition negative and positive in {1, -1, 2, -2, 3, -3, 4, -4, 5, -5}" << std::endl;
    SList<int> ListToPartition = { 1, -1, 2, -2, 3, -3, 4, -4, 5, -5 };
    auto isPositiveLambda = [](SList<int>::node_type node)->bool { return node >= 0; };
    std::partition(ListToPartition.begin(), ListToPartition.end(), isPositiveLambda);
    for (const SList<int>::node_type& node : ListToPartition) {
        std::cout << node << " ";
    }
    std::cout << std::endl;

    std::cout << "std::merge on {1.2f, 3.2f, 4.9999f, 0.f} and the previously copied vector" << std::endl;
    std::vector<SList<float>::node_type> mergeResult;

    listAllLessThan5.sort();
    std::sort(destinationVector.begin(), destinationVector.end());

    std::merge(listAllLessThan5.begin(), listAllLessThan5.end(), destinationVector.begin(), destinationVector.end(), std::back_inserter(mergeResult));
    for (const SList<float>::node_type& node : mergeResult) {
        std::cout << node.value << " ";
    }
    std::cout << std::endl;

    std::cout << "min max on {1, -1, 2, -2, 3, -3, 4, -4, 5, -5}" << std::endl;
    auto pairResult = std::minmax_element(ListToPartition.begin(), ListToPartition.end());
    std::cout << "Min: " << *pairResult.first << " Max: " << *pairResult.second << std::endl;


    std::cout << "std::accumulate on {1, -1, 2, -2, 3, -3, 4, -4, 5, -5}" << std::endl;
    auto sumOp = [](const SList<int>::node_type& n1, const SList<int>::node_type& n2)->int {return n1.value + n2.value; };
    int sum = std::accumulate(ListToPartition.cbegin(), ListToPartition.cend(), 0, sumOp);
    std::cout << "Sum: " << sum << std::endl;


    std::cout << "std::set_union on {1, 2, 3, 4, 5} and {3, 4, 5, 6, 7}" << std::endl;
    SList<int> set1 = { 1, 2, 3, 4, 5 };
    SList<int> set2 = { 3, 4, 5, 6, 7 };

    SList<int> set3;
    std::merge(set1.begin(), set1.end(), set2.begin(), set2.end(), std::front_inserter(set3));

    std::vector<SList<int>::node_type> result;
    std::set_union(set1.cbegin(), set1.cend(), set2.cbegin(), set2.cend(), std::back_inserter(result));
    for (const SList<int>::node_type& node : result) {
        std::cout << node << " ";
    }
    std::cout << std::endl;

    std::cout << "std::set_difference on {1, 2, 3, 4, 5} and {3, 4, 5, 6, 7}" << std::endl;
    result.clear();
    std::set_difference(set1.cbegin(), set1.cend(), set2.cbegin(), set2.cend(), std::back_inserter(result));
    for (const SList<int>::node_type& node : result) {
        std::cout << node << " ";
    }
    std::cout << std::endl;

    std::cout << "std::set_intersection on {1, 2, 3, 4, 5} and {3, 4, 5, 6, 7}" << std::endl;
    result.clear();
    std::set_intersection(set1.cbegin(), set1.cend(), set2.cbegin(), set2.cend(), std::back_inserter(result));
    for (const SList<int>::node_type& node : result) {
        std::cout << node << " ";
    }
    std::cout << std::endl;

    std::cout << "std::set_symmetric_difference on {1, 2, 3, 4, 5} and {3, 4, 5, 6, 7}" << std::endl;
    result.clear();
    std::set_symmetric_difference(set1.cbegin(), set1.cend(), set2.cbegin(), set2.cend(), std::back_inserter(result));
    for (const SList<int>::node_type& node : result) {
        std::cout << node << " ";
    }
    std::cout << std::endl;

    std::cout << "Swapping set1 and set2 with std::swap overload" << std::endl;
    std::swap(set1, set2);
    std::cout << "Set1: ";
    for (const SList<int>::node_type& node1 : set1) {
        std::cout << node1 << " ";
    }
    std::cout << std::endl << "Set2: ";
    for (const SList<int>::node_type& node2 : set2) {
        std::cout << node2 << " ";
    }

    SList<int> listaDaCopiareUno = { -1,-2 };
    SList<int> listaDaCopiareDue = { 4,1,2,3,4,55 };

    std::copy(listaDaCopiareUno.begin(), listaDaCopiareUno.end(), listaDaCopiareDue.begin());

    auto iteratoreCopiato = listaDaCopiareDue.begin();

    std::cout << "Copy of { -1,-2 } in { 4,1,2,3,4,55 }\n";

    while (iteratoreCopiato != listaDaCopiareDue.end()) {
        std::cout << *iteratoreCopiato << " ";
        iteratoreCopiato++;
    }

    std::cout << "\n";

    set1= { 1,2,1,1,3,3,3,4,5,4 };
    auto endUnique = std::unique(set1.begin(), set1.end());
    auto uniqueIterator = set1.begin();
    while (uniqueIterator != endUnique) {
        std::cout << *uniqueIterator << " ";
            uniqueIterator++;
    }
    std::cout << std::endl;


    return;
}
void SListArrayTesting() 
{
    using namespace CoolKidzList;
    //====SETUP=====
    SListArray<int> sListArray; 
    sListArray.push_front(69);
    sListArray.push_front(42);
    sListArray.push_front(666);
    std::cout<<sListArray<<std::endl;

    sListArray.pop_front();
    sListArray.pop_front();
    sListArray.push_front(13);
    sListArray.push_front(07);
    sListArray.push_front(1997);
    std::cout << sListArray << std::endl;


    auto itBegin = sListArray.cbegin();
    sListArray.insert_after(itBegin + 1, 420);
    sListArray.insert_after(sListArray.before_begin(), -666);
    sListArray.push_front(600000666);
    sListArray.erase_after(itBegin);
    std::cout<<"Inserting 420 after pos [1], erasing after [0]"<<std::endl<<sListArray << std::endl;

    SListArray<int> sListArray2 = {-1, -2, -3, -4};
    std::swap(sListArray, sListArray2);
    std::cout<<"Swapping the created list with {-1, -2, -3, -4}"<<std::endl<<"Previous List: "<<sListArray << std::endl <<"Just created list:"<< sListArray2 << std::endl;

    std::cout<<"Clearing and adding some new elements"<<std::endl;
    sListArray2.clear();
    sListArray2.push_front(3);
    sListArray2.push_front(4);
    std::cout<<sListArray2<<std::endl;


    //limit case test
    SListArray<std::string> sListArrangers = {"In","this","town","lived","an","outlaw","by","the","name","of","Texas","Red"};
    sListArrangers.erase_after(sListArrangers.before_begin());
    sListArrangers.insert_after(sListArrangers.before_begin(), "At");
    auto red = sListArrangers.begin() + 10;
    sListArrangers.erase_after(red);
    sListArrangers.insert_after(red, ".");
    std::cout << sListArrangers << std::endl;


    std::cout<<"SORTING"<<std::endl;
    sListArray2 = SListArray<int>({1, 2, 41, -1, 3});
    sListArray2.sort();
    std::cout<<sListArray2<< std::endl;

    //=======Algoritmi STL=========
    std::fill(sListArray2.before_begin() + 2, sListArray2.end(), 213);
    std::cout << sListArray2<< std::endl;

    sListArray2 = { 1, 60};
    sListArray = { 5,3,5,5,5 };
	std::swap(sListArray, sListArray2);
	std::cout << "Swapping the created list with {-1, -2, -3, -4}" << std::endl << "Previous List: " << sListArray << std::endl << "Just created list:" << sListArray2 << std::endl;

    sListArray2.sort();
    sListArray.sort();

    std::vector<NodeArray<int>> desti;
    SListArray<int> SListArray4;
 	std::merge(sListArray.begin(), sListArray.end(), sListArray2.begin(), sListArray2.end(), std::front_inserter(SListArray4));
 	std::merge(sListArray.begin(), sListArray.end(), sListArray2.begin(), sListArray2.end(), std::back_inserter(desti));
	std::cout << "Merging the two previous list ";
    for (const NodeArray<int>& n : desti) {
        std::cout<<n.value<<" ";
    }
    std::cout<<std::endl;

    sListArray = {-10, -20};
    sListArray2 = {1, 2, 3, 4, 5, 6};
    std::copy(sListArray.begin(), sListArray.end(), sListArray2.begin());
    std::cout<<"Copying {-10, -20} into {1, 2, 3, 4, 5, 6}"<<std::endl<<sListArray2<<std::endl;

    std::cout << "Copying {-10, -20, 3, 4, 5, 6} into empty SList with std::front_inserter (list will be backward)" << std::endl;
    SListArray<int> destinationList;
    std::copy(sListArray2.begin(), sListArray2.end(), std::front_inserter(destinationList));
    std::cout<<destinationList<<std::endl;

	std::cout << "Copying {-10, -20, 3, 4, 5, 6} into empty SList preserving order with std::distance" << std::endl;
	destinationList = SListArray<int>(std::distance(sListArray2.begin(), sListArray2.end()));
	std::copy(sListArray2.begin(), sListArray2.end(), destinationList.begin());
	std::cout << destinationList << std::endl;

	sListArray = { 1, 4,23 };
	sListArray2 = { 1, 2, 3, 4, 5, 6 };
    SListArray4 = { };

    std::set_intersection(sListArray.begin(), sListArray.end(), sListArray2.begin(), sListArray2.end(), std::front_inserter(SListArray4));

    std::cout << "set_intersection of { 1, 4,23 } and { 1, 2, 3, 4, 5, 6 }: " << std::endl;
    std::cout << SListArray4 << std::endl;

    sListArray = { 1, 4,23,7234, 234, 90, int('*')};
    std::cout << "Max element between { 1, 4,23,7234, 234, 90, 42} is " << *std::max_element(sListArray.begin(), sListArray.end())<< std::endl;

    sListArray = { 1, 4,23,7234, 234, 90, int('*'), -69000};
    std::cout << "Min element between { 1, 4,23,7234, 234, 90, 42, -69000} is " << *std::min_element(sListArray.begin(), sListArray.end()) << std::endl;

	sListArray = { 0, 1, 2, 3, 40, 40, 41, 41, 5 };

	auto i1 = std::adjacent_find(sListArray.begin(), sListArray.end());

	if (i1 == sListArray.end()) {
		std::cout << "No matching adjacent elements\n";
	}
	else {
		std::cout << "The first adjacent pair of equal elements in { 0, 1, 2, 3, 40, 40, 41, 41, 5 } is at "
			<< std::distance(sListArray.begin(), i1) << ", *i1 = "
			<< *i1 << '\n';
	}

	auto i2 = std::adjacent_find(sListArray.begin(), sListArray.end(), std::greater<int>());
	if (i2 == sListArray.end()) {
		std::cout << "The entire vector is sorted in ascending order\n";
	}
	else {
		std::cout << "The last element in the non-decreasing subsequence in { 0, 1, 2, 3, 40, 40, 41, 41, 5 } is at "
			<< std::distance(sListArray.begin(), i2) << ", *i2 = " << *i2 << '\n';
	}

	std::generate(sListArray.begin(), sListArray.end(), [n = 0]() mutable { return n++; });
    std::cout << sListArray << std::endl;
    
    sListArray = { 1,2,1,1,3,3,3,4,5,4 };
    auto iteratorCancell = std::unique(sListArray.begin(),sListArray.end());
    size_t dist = std::distance(sListArray.begin(), iteratorCancell);
    auto iteratoreDaCuiCancell = sListArray.begin() + int(dist - 1);
    for (int i = 0; i <3; i++) {
        sListArray.erase_after(iteratoreDaCuiCancell);
    }
    std::cout << "unique for { 1,2,1,1,3,3,3,4,5,4 }: " << sListArray << std::endl;

    SListArray<std::string> casualStrings;

    //il codice di sotto non si pu� fare perch� non siamo in c++20 :(
    //std::cout << "3 casual strings from \" "<< sListArrangers<<"\": "<< std::sample(sListArrangers.begin(), sListArrangers.end(), std::back_inserter(casualStrings)) ;
}

void FixedSListTesting()
{
	using namespace CoolKidzList;
	//====SETUP=====
	FixedSList<int, 44> sListArray;
	sListArray.push_front(69);
	sListArray.push_front(42);
	sListArray.push_front(666);
	std::cout << sListArray << std::endl;

	sListArray.pop_front();
	sListArray.pop_front();
	sListArray.push_front(13);
	sListArray.push_front(07);
	sListArray.push_front(1997);
	std::cout << sListArray << std::endl;

    //IT JUST WORKS
	auto itBegin = sListArray.cbegin();
	sListArray.insert_after(itBegin + 1, 420);
	sListArray.insert_after(sListArray.before_begin(), -666);
	sListArray.push_front(600000666);
	sListArray.erase_after(itBegin);
	std::cout << "Inserting 420 after pos [1], erasing after [0]" << std::endl << sListArray << std::endl;

	FixedSList<int, 44> sListArray2 = { -1, -2, -3, -4 };
	std::swap(sListArray, sListArray2);
	std::cout << "Swapping the created list with {-1, -2, -3, -4}" << std::endl << "Previous List: " << sListArray << std::endl << "Just created list:" << sListArray2 << std::endl;

	std::cout << "Clearing and adding some new elements" << std::endl;
	sListArray2.clear();
	sListArray2.push_front(3);
	sListArray2.push_front(4);
	std::cout << sListArray2 << std::endl;


	//limit case test
	FixedSList<std::string, 12> sListArrangers = { "In","this","town","lived","an","outlaw","by","the","name","of","Texas","Red" };
	sListArrangers.erase_after(sListArrangers.before_begin());
	sListArrangers.insert_after(sListArrangers.before_begin(), "At");
	auto red = sListArrangers.begin() + 10;
	sListArrangers.erase_after(red);
	sListArrangers.insert_after(red, ".");
	std::cout << sListArrangers << std::endl;

	std::cout << "SORTING" << std::endl;
	sListArray2 = FixedSList<int, 44>({ 1, 2, 41, -1, 3 });
	sListArray2.sort();
	std::cout << sListArray2 << std::endl;

	//=======Algoritmi STL=========
	std::fill(sListArray2.before_begin() + 2, sListArray2.end(), 213);
	std::cout << sListArray2 << std::endl;

 	sListArray2 = { 1, 60 };
 	sListArray = { 5,3,5,5,5 };
 	std::swap(sListArray, sListArray2);
 	std::cout << "Swapping the created list with {-1, -2, -3, -4}" << std::endl << "Previous List: " << sListArray << std::endl << "Just created list:" << sListArray2 << std::endl;


	std::vector<NodeArray<int>> desti;
	FixedSList<int, 20> FixedSList4;
    sListArray = { 3,5,5,5,5 };
    sListArray2 = { 1, 60 };
	std::merge(sListArray.begin(), sListArray.end(), sListArray2.begin(), sListArray2.end(), std::front_inserter(FixedSList4));
	std::merge(sListArray.begin(), sListArray.end(), sListArray2.begin(), sListArray2.end(), std::back_inserter(desti));
	std::cout << "Merging the two previous list ";
	for (const NodeArray<int>& n : desti) {
		std::cout << n.value << " ";
	}
	std::cout << std::endl;

	sListArray = { -10, -20 };
	sListArray2 = { 1, 2, 3, 4, 5, 6 };
	std::copy(sListArray.begin(), sListArray.end(), sListArray2.begin());
	std::cout << "Copying {-10, -20} into {1, 2, 3, 4, 5, 6}" << std::endl << sListArray2 << std::endl;

	std::cout << "Copying {-10, -20, 3, 4, 5, 6} into empty SList with std::front_inserter (list will be backward)" << std::endl;
	FixedSList<int, 20> destinationList;
	std::copy(sListArray2.begin(), sListArray2.end(), std::front_inserter(destinationList));
	std::cout << destinationList << std::endl;

	sListArray = { 1, 4,23 };
	sListArray2 = { 1, 2, 3, 4, 5, 6 };
	FixedSList<int,42> fixedSListArray4 = { };

	std::set_intersection(sListArray.begin(), sListArray.end(), sListArray2.begin(), sListArray2.end(), std::front_inserter(fixedSListArray4));

	std::cout << "set_intersection of { 1, 4,23 } and { 1, 2, 3, 4, 5, 6 }: " << std::endl;
	std::cout << fixedSListArray4 << std::endl;

	sListArray = { 1, 4,23,7234, 234, 90, int('*') };
	std::cout << "Max element between { 1, 4,23,7234, 234, 90, 42, -69000} is " << *std::max_element(sListArray.begin(), sListArray.end()) << std::endl;

	sListArray = { 1, 4,23,7234, 234, 90, int('*'), -69000 };
	std::cout << "Min element between { 1, 4,23,7234, 234, 90, 42, -69000} is " << *std::min_element(sListArray.begin(), sListArray.end()) << std::endl;

	sListArray = { 0, 1, 2, 3, 40, 40, 41, 41, 5 };

	auto i1 = std::adjacent_find(sListArray.begin(), sListArray.end());

	if (i1 == sListArray.end()) {
		std::cout << "No matching adjacent elements\n";
	}
	else {
		std::cout << "The first adjacent pair of equal elements in { 0, 1, 2, 3, 40, 40} is at "
			<< std::distance(sListArray.begin(), i1) << ", *i1 = "
			<< *i1 << '\n';
	}

	auto i2 = std::adjacent_find(sListArray.begin(), sListArray.end(), std::greater<int>());
	if (i2 == sListArray.end()) {
		std::cout << "The entire vector is sorted in ascending order\n";
	}
	else {
		std::cout << "The last element in the non-decreasing subsequence in { 0, 1, 2, 3, 40, 40, 41, 41, 5 }  is at "
			<< std::distance(sListArray.begin(), i2) << ", *i2 = " << *i2 << '\n';
	}

	std::generate(sListArray.begin(), sListArray.end(), [n = 0]() mutable { return n++; });
	std::cout << "Filling with increasing numbers with std::generate()\n" << sListArray << std::endl;

	sListArray = { 1,2,1,1,3,3,3,4,5,4 };
	auto iteratorCancell = std::unique(sListArray.begin(), sListArray.end());
	size_t dist = std::distance(sListArray.begin(), iteratorCancell);
	auto iteratoreDaCuiCancell = sListArray.begin() + int(dist - 1);
	for (int i = 0; i < 3; i++) {
		sListArray.erase_after(iteratoreDaCuiCancell);
	}
	std::cout << "unique for { 1,2,1,1,3,3,3,4,5,4 }: " << sListArray << std::endl;

	FixedSList<std::string,3> casualStrings;

	//il codice di sotto non si pu� fare perch� non siamo in c++20 :(
	//std::cout << "3 casual strings from \" "<< sListArrangers<<"\": "<< std::sample(sListArrangers.begin(), sListArrangers.end(), std::back_inserter(casualStrings)) ;

}