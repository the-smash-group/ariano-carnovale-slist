#pragma once

#include<vector>
#include<stack>
#include <iostream>
#include <algorithm>
#include "NodeArray.h"

namespace CoolKidzList {

	template<typename T>
	class SListArray
	{
	public:

		//==========Iterators class Declarations==========
		using value_type = T; //Needed by STL
		using node_type = NodeArray<T>;
		static constexpr int POSBEFOREHEAD = NodeArray<T>::POSBEFOREHEAD;
		static constexpr int POSDELETED = NodeArray<T>::POSDELETED;
		static constexpr int POSEND = NodeArray<T>::POSEND;


		struct SListArrayIterator {
			using iterator_category = std::forward_iterator_tag;
			using difference_type = size_t;
			using value_type = SListArray::node_type;
			using pointer = value_type*;
			using reference = SListArray::value_type&;

			SListArrayIterator() = delete;
			SListArrayIterator(std::vector<node_type>* pointed_vector) :vector(pointed_vector), position(0) {};
			SListArrayIterator(std::vector<node_type>* pointed_vector, int pos) :vector(pointed_vector), position(pos) {};
			SListArrayIterator(const SListArrayIterator& other) : vector(other.vector), position(other.position) {}

			SListArrayIterator& operator=(const SListArrayIterator& other){if(*this != other)
				{
					this->vector = other.vector; 
					this->position = other.position; 
				}	
				return *this;
			}

			reference operator*() const { return (vector->at(position).value); }
			pointer operator->() const { return &(vector->at(position)); }

			SListArrayIterator& operator++() { position = (*this)->nextIndex; return *this; }
			SListArrayIterator operator++(int) { SListArrayIterator tmp = *this; ++(*this); return tmp; }
			SListArrayIterator operator+(int right) {
				SListArrayIterator tmp = *this;
				for (int i = 0; i < right; i++) tmp++;
				return tmp;
			}

			friend bool operator==(const SListArrayIterator& a, const SListArrayIterator& b) { 
				//Two iterators are equal if they point to the same vector at the same position
				return (a.vector == b.vector) && (a.position == b.position);
			};
			friend bool operator!=(const SListArrayIterator& a, const SListArrayIterator& b) { 
				return !(a==b);
			}

			std::vector<node_type>* vector;
			int position;
		};
		
		struct SListArrayConstIterator {
			using iterator_category = std::forward_iterator_tag;
			using difference_type = size_t;
			using value_type = SListArray::node_type;
			using pointer = const value_type*;
			using reference = const SListArray::value_type&;

			SListArrayConstIterator() = delete;
			SListArrayConstIterator(const std::vector<node_type>* pointed_vector) :vector(pointed_vector), position(0) {};
			SListArrayConstIterator(const std::vector<node_type>* pointed_vector, int pos) :vector(pointed_vector), position(pos) {};
			SListArrayConstIterator(const SListArrayConstIterator& other) : vector(other.vector), position(other.position) {}
			SListArrayConstIterator(const SListArrayIterator& other) : vector(other.vector), position(other.position) {}

			SListArrayConstIterator& operator=(const SListArrayConstIterator& other) {
				if (*this != other)
				{
					this->vector = other.vector;
					this->position = other.position;
				}
				return *this;
			}

			reference operator*() const { return (vector->at(position).value); }
			pointer operator->() const { return &(vector->at(position)); }

			SListArrayConstIterator& operator++() { position = (*this)->nextIndex; return *this; }
			SListArrayConstIterator operator++(int) { SListArrayConstIterator tmp = *this; ++(*this); return tmp; }
			SListArrayConstIterator operator+(int right) {
				SListArrayConstIterator tmp = *this;
				for (int i = 0; i < right; i++) tmp++;
				return tmp;
			}

			friend bool operator==(const SListArrayConstIterator& a, const SListArrayConstIterator& b) { 
				return (a.vector == b.vector) && (a.position == b.position);
			};
			friend bool operator!=(const SListArrayConstIterator& a, const SListArrayConstIterator& b) { 
				return !(a==b);
			}

			const std::vector<node_type>* vector;
			int position;
		};
		
		using iterator = SListArrayIterator;
		using const_iterator = SListArrayConstIterator;


		//==========Constructors==========
		SListArray();
		SListArray(size_t count, const T& value);
		SListArray(size_t count);
		template <class InputIterator,
			std::enable_if_t<std::_Is_iterator_v<InputIterator>, bool> = true >
		SListArray(InputIterator first, InputIterator last);
		SListArray(std::initializer_list<T> init);
		SListArray(const SListArray& other);

		//==========Destructor==========
		~SListArray() = default;

		//==========Iterators==========
		iterator before_begin() noexcept {iterator beforeBeginIt = iterator(&m_data, POSBEFOREHEAD); return beforeBeginIt;};
		const_iterator before_begin() const noexcept { const_iterator beforeBeginIt = const_iterator(&m_data, POSBEFOREHEAD); return beforeBeginIt; };
		const_iterator cbefore_begin() const noexcept { return before_begin();};

		inline iterator begin() noexcept { iterator beginIt = iterator(&m_data, posHead); return beginIt; };
		inline const_iterator begin() const noexcept { const_iterator beginIt = const_iterator(&m_data, posHead); return beginIt; };
		inline const_iterator cbegin() const noexcept { return begin(); };

		inline iterator end() noexcept { iterator endIt = iterator(&m_data, POSEND); return endIt; };
		inline const_iterator end() const noexcept { const_iterator endIt(&m_data, POSEND); return endIt; };
		inline const_iterator cend() const noexcept { return end();};
		
		//==========Modifiers==========
		void clear() noexcept;
		iterator insert_after(const_iterator pos, const T& value);
		iterator erase_after(const_iterator pos);
		void push_front(const T& value);
		void pop_front();
		void swap(SListArray& other);

		//=========Operations==========
		void sort();
		template<class Compare>
		void sort(Compare comp);

		//=========Operators==========
		friend std::ostream& operator<<(std::ostream& os, const SListArray& list) 
		{
			const_iterator it = list.begin();
			for ( ;it != list.end(); it++) {
				std::cout<<*it<<" ";
			}
			return os;
		}
		
	private:
		std::stack<int> freePositionInVector;
		std::vector<node_type> m_data;
		int posHead;
		int posTail;

		//=========UTILITY=============
		iterator _push_front_internal(const T& value);
	};


#pragma region DEFINITIONS

template<typename T>
SListArray<T>::SListArray():
	posHead(-1),
	posTail(1),
	m_data(),
	freePositionInVector()
{
	m_data.push_back(node_type(T(), POSEND));
}

template<typename T>
SListArray<T>::SListArray(size_t count, const T& value) : SListArray() {
	for (int i = 0; i < count; i++) {
		push_front(value);
	}
}

template<typename T>
SListArray<T>::SListArray(size_t count) : SListArray(count, T()) {}

template<typename T>
template<class InputIterator, std::enable_if_t<std::_Is_iterator_v<InputIterator>, bool>>
SListArray<T>::SListArray(InputIterator first, InputIterator last) : SListArray()
{
	auto it = cbefore_begin();
	int insertedPosition = 1;
	for (;first != last; first++, it++) {
		auto insertedIt = insert_after(it, *first);
		insertedPosition = insertedIt.position;
	}
	posHead = cbefore_begin()->nextIndex;
	posTail = insertedPosition;
}

template<typename T>
SListArray<T>::SListArray(std::initializer_list<T> init) : SListArray(init.begin(), init.end()) {}

template<typename T>
void SListArray<T>::clear() noexcept{
	auto it = begin();
	while (it != end()) {
		freePositionInVector.push(it.position);
		auto next = it+1;
		it->nextIndex = -1;
		it = next;
	}

	posHead = POSEND;
	posTail = 1;
}

template<typename T>
SListArray<T>::SListArray(const SListArray<T>& other) : 
	posHead(other.posHead),
	posTail(other.posTail),
	m_data(other.m_data),
	freePositionInVector(other.freePositionInVector)
	{}

template<typename T>
inline typename SListArray<T>::iterator SListArray<T>::insert_after(const_iterator pos, const T& value)
{
	
	int posToInsert = POSEND;
	if (freePositionInVector.size() > 0) {
		posToInsert = freePositionInVector.top();
		freePositionInVector.pop();
		m_data[posToInsert].value = value;		
	}
	else {
		auto inserted = _push_front_internal(value);
		posToInsert = inserted.position;
	}

	if (pos.position == 0) {
		posHead = posToInsert;
	}

	m_data[posToInsert].nextIndex = pos->nextIndex;
	pos->nextIndex = posToInsert;

	if (m_data[posToInsert].nextIndex == POSEND)
		posTail = posToInsert;
	
	return iterator(&m_data, posToInsert);
}

template<typename T>
inline typename SListArray<T>::iterator SListArray<T>::erase_after(const_iterator pos)
{
	if (pos->nextIndex != POSEND) {
		auto nextNode = pos + 1;
		freePositionInVector.push(pos->nextIndex);
		if (pos->nextIndex == posHead)
			posHead = nextNode->nextIndex;
		if (pos->nextIndex == posTail)
			posTail = pos.position;
		pos->nextIndex = nextNode->nextIndex;
		nextNode->nextIndex = POSDELETED;
	};

	return iterator(&m_data, pos->nextIndex);
}

template <typename T>
void SListArray<T>::push_front(const T& value) 
{	
	if (freePositionInVector.size() > 0) 
	{
		int posToInsert = freePositionInVector.top();
		freePositionInVector.pop();
		m_data[posToInsert].value = value;
		m_data[posToInsert].nextIndex = posHead;
		if (posHead == -1)
			posTail = posToInsert;
		posHead = posToInsert;
		m_data[POSBEFOREHEAD].nextIndex = posHead;
	}
	else 
	{
		_push_front_internal(value);
		posHead = int(m_data.size() - 1);
		m_data[POSBEFOREHEAD].nextIndex = posHead;
	}
}

template<typename T>
inline void SListArray<T>::pop_front()
{
	if (posHead!=-1) {
		freePositionInVector.push(posHead);
		int posToGo = m_data[posHead].nextIndex;
		m_data[posHead].nextIndex = -1;
		posHead = posToGo;
		if (posHead!= -1 && m_data[posHead].nextIndex == POSEND) {
			posTail = posHead;
		}
		m_data[POSBEFOREHEAD].nextIndex = posHead;
	}
}

template<typename T>
void SListArray<T>::swap(SListArray<T>& other) {
	std::swap(m_data, other.m_data);
	std::swap(freePositionInVector, other.freePositionInVector);
	std::swap(posHead, other.posHead);
	std::swap(posTail, other.posTail);
}

template<typename T>
void SListArray<T>::sort() {
	auto _LESS = [](const node_type& a, const node_type& b)->bool {return a < b; };
	sort(_LESS);
}

template<typename T>
template<class Compare>
void SListArray<T>::sort(Compare comp) {
	if (m_data.size() <= 2) {
		return;
	}
	std::sort(m_data.begin()+1, m_data.end(), comp);
	auto itToUpdate = m_data.cbegin();
	auto itToNext = itToUpdate;
	while (itToUpdate != m_data.end() && itToNext != m_data.end()) {
		
		if(itToUpdate->nextIndex == POSDELETED) { itToUpdate++; itToNext = itToUpdate; continue;}
		if(itToNext->nextIndex == POSDELETED) {itToNext++; continue;};

		itToUpdate->nextIndex = int(std::distance(m_data.cbegin(), itToNext));
		itToUpdate = itToNext;
		itToNext++;
	}

	posHead = cbefore_begin()->nextIndex;
	posTail = int(std::distance(m_data.cbegin()+1, itToNext));
	itToUpdate->nextIndex = POSEND;

}

template<typename T>
typename SListArray<T>::iterator SListArray<T>::_push_front_internal(const T& value)
{
	node_type nodeToInsert;
	if (posHead == -1)
	{
		nodeToInsert = node_type(value, POSEND);
		posTail = int(m_data.size());
	}
	else {
		nodeToInsert = node_type(value, posHead);
	}
	m_data.push_back(nodeToInsert);

	return iterator(&m_data, int(m_data.size()-1));
}

#pragma endregion DEFINITIONS
}

namespace std {
	using CoolKidzList::SListArray;

	template<typename T>
	void swap(SListArray<T>& lhs, SListArray<T>& rhs) noexcept { lhs.swap(rhs); };

}